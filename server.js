/**
 * @file node js http服务器
 *
 * @author: 元实
 */

/**
 * 1. 2021-07-01 元实 初始版本
 */

 const express = require('express')
 const app = express()
 
 // Укажем директорию в которой будут лежать наши файлы
 app.use(express.static('public'))
 
 // Отправляем index.html, когда пользователи получают доступ к
 // корневому каталог с использованием res.sendFile()
 app.get('/', (req, res) => {
   res.sendFile(__dirname + '/public/index.html')
 })
 
 app.listen(8090, () => console.log('Listening to the port 8090'))
